Vector2   = {}
Vector2.x = 0
Vector2.y = 0

function Vector2.new(x, y, z)
  local obj = deepcopy(Vector2)
  obj:set(x or 0, y or 0, z or 0)
  return obj
end

function Vector2:empty()
  if self then
    return self.x == 0 and self.y == 0
  end
  return true
end

function Vector2:length_sqr()
  if self then
    return self.x * self.x + self.y * self.y
  end
  return nil
end

function Vector2:length()
  if self then
    return math.sqrt(self:length_sqr())
  end
  return nil
end

function Vector2:normalize()
  if self then
    local l = self:length()
    self.x = self.x / l
    self.y = self.y / l
  end
end

function Vector2:normalized()
  if self then
    local obj = deepcopy(self)
    obj:normalize()
    return obj
  end
  return nil
end

function Vector2:dot(rhs)
  if self then
    return self.x * rhs.x + self.y * rhs.y
  end
  return nil
end

function Vector2:inverse()
  if self then
    self.x = 1 / self.x
    self.y = 1 / self.y
  end
end

function Vector2:inversed()
  if self then
    local obj = deepcopy(self)
    obj:inverse()
    return obj
  end
  return nil
end

function Vector2:distance(rhs)
  if self then
    local delta = rhs - self
    return delta:length()
  end
  return nil
end

function Vector2:set(x, y)
  if self then
    self.x = x or 0
    self.y = y or 0
  end
end

function Vector2:fill(value)
  if self then
    self:set(value, value)
  end
end

function Vector2:read(address)
  if self then
    local read_float = Process.read_float

    self.x = read_float(address)
    self.y = read_float(address + 0x4)
  end
end

function Vector2:write(address)
  if self then
    local write_float = Process.write_float

    local wpm1 = write_float(address,       self.x)
    local wpm2 = write_float(address + 0x4, self.y)

    return wpm1 and wpm2
  end
  return false
end

setmetatable(Vector2, {
  __add = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x + rhs
      obj.y = obj.y + rhs
    else
      obj.x = obj.x + rhs.x
      obj.y = obj.y + rhs.y
    end
    return obj
  end,
  __sub = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x - rhs
      obj.y = obj.y - rhs
    else
      obj.x = obj.x - rhs.x
      obj.y = obj.y - rhs.y
    end
    return obj
  end,
  __mul = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x * rhs
      obj.y = obj.y * rhs
    else
      obj.x = obj.x * rhs.x
      obj.y = obj.y * rhs.y
    end
    return obj
  end,
  __div = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x / rhs
      obj.y = obj.y / rhs
    else
      obj.x = obj.x / rhs.x
      obj.y = obj.y / rhs.y
    end
    return obj
  end,
  __tostring = function(lhs)
    return string.format('%.4f | %.4f', lhs.x, lhs.y)
  end
})

Vector3   = {}
Vector3.x = 0
Vector3.y = 0
Vector3.z = 0

function Vector3.new(x, y, z)
  local obj = deepcopy(Vector3)
  obj.x = x or 0
  obj.y = y or 0
  obj.z = z or 0
  return obj
end

function Vector3:empty()
  if self then
    return self.x == 0 and self.y == 0 and self.z == 0
  end
  return true
end

function Vector3:length_sqr()
  if self then
    return self.x * self.x + self.y * self.y + self.z * self.z
  end
  return nil
end

function Vector3:length()
  if self then
    return math.sqrt(self:length_sqr())
  end
  return nil
end

function Vector3:normalize()
  if self then
    local l = self:length()
    self.x = self.x / l
    self.y = self.y / l
    self.z = self.z / l
  end
end

function Vector3:normalized()
  if self then
    local obj = deepcopy(self)
    obj:normalize()
    return obj
  end
  return nil
end

function Vector3:dot(rhs)
  if self then
    return self.x * rhs.x + self.y * rhs.y + self.z * rhs.z
  end
  return nil
end

function Vector3:cross(rhs)
  if self then
    return Vector3.new(
      self.y * rhs.z - self.z * rhs.y,
      self.z * rhs.x - self.x * rhs.z,
      self.x * rhs.y - self.y * rhs.x
    )
  end
  return nil
end

function Vector3:ncross(rhs)
  if self then
    local obj = self:cross(rhs)
    obj:normalize()
    return obj
  end
  return nil
end

function Vector3:inverse()
  if self then
    self.x = 1 / self.x
    self.y = 1 / self.y
    self.z = 1 / self.z
  end
end

function Vector3:inversed()
  if self then
    local obj = deepcopy(self)
    obj:inverse()
    return obj
  end
  return nil
end

function Vector3:distance(rhs)
  if self then
    local delta = rhs - self
    return delta:length()
  end
  return nil
end

function Vector3:set(x, y, z)
  if self then
    self.x = x or 0
    self.y = y or 0
    self.z = z or 0
  end
end

function Vector3:fill(value)
  if self then
    self:set(value, value, value)
  end
end

function Vector3:read(address)
  if self then
    local read_float = Process.read_float

    self.x = read_float(address)
    self.y = read_float(address + 0x4)
    self.z = read_float(address + 0x8)
  end
end

function Vector3:write(address)
  if self then
    local write_float = Process.write_float

    local wpm1 = write_float(address,       self.x)
    local wpm2 = write_float(address + 0x4, self.y)
    local wpm3 = write_float(address + 0x8, self.z)

    return wpm1 and wpm2 and wpm3
  end
  return false
end

setmetatable(Vector3, {
  __add = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x + rhs
      obj.y = obj.y + rhs
      obj.z = obj.z + rhs
    else
      obj.x = obj.x + rhs.x
      obj.y = obj.y + rhs.y
      obj.z = obj.z + rhs.z
    end
    return obj
  end,
  __sub = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x - rhs
      obj.y = obj.y - rhs
      obj.z = obj.z - rhs
    else
      obj.x = obj.x - rhs.x
      obj.y = obj.y - rhs.y
      obj.z = obj.z - rhs.z
    end
    return obj
  end,
  __mul = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x * rhs
      obj.y = obj.y * rhs
      obj.z = obj.z * rhs
    else
      obj.x = obj.x * rhs.x
      obj.y = obj.y * rhs.y
      obj.z = obj.z * rhs.z
    end
    return obj
  end,
  __div = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x / rhs
      obj.y = obj.y / rhs
      obj.z = obj.z / rhs
    else
      obj.x = obj.x / rhs.x
      obj.y = obj.y / rhs.y
      obj.z = obj.z / rhs.z
    end
    return obj
  end,
  __tostring = function(lhs)
    return string.format('%.4f | %.4f | %.4f', lhs.x, lhs.y, lhs.z)
  end
})

Vector4   = {}
Vector4.x = 0
Vector4.y = 0
Vector4.z = 0
Vector4.w = 0

function Vector4.new(x, y, z, w)
  local obj = deepcopy(Vector4)
  obj:set(x or 0, y or 0, z or 0, w or 0)
  return obj
end

function Vector4:empty()
  if self then
    return self.x == 0 and self.y == 0 and self.z == 0 and self.w == 0
  end
  return true
end

function Vector4:length_sqr()
  if self then
    return self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w
  end
  return nil
end

function Vector4:length()
  if self then
    return math.sqrt(self:length_sqr())
  end
  return nil
end

function Vector4:normalize()
  if self then
    local l = self:length()
    self.x = self.x / l
    self.y = self.y / l
    self.z = self.z / l
    self.w = self.w / l
  end
end

function Vector4:normalized()
  if self then
    local obj = deepcopy(self)
    obj:normalize()
    return obj
  end
  return nil
end

function Vector4:dot(rhs)
  if self then
    return self.x * rhs.x + self.y * rhs.y + self.z * rhs.z + self.w * self.w
  end
  return nil
end

function Vector4:inverse()
  if self then
    self.x = 1 / self.x
    self.y = 1 / self.y
    self.z = 1 / self.z
    self.w = 1 / self.w
  end
end

function Vector4:inversed()
  if self then
    local obj = deepcopy(self)
    obj:inverse()
    return obj
  end
  return nil
end

function Vector4:distance(rhs)
  if self then
    local delta = rhs - self
    return delta:length()
  end
  return nil
end

function Vector4:set(x, y, z, w)
  if self then
    self.x = x or 0
    self.y = y or 0
    self.z = z or 0
    self.w = w or 0
  end
end

function Vector4:fill(value)
  if self then
    self:set(value, value, value, value)
  end
end

function Vector4:read(address)
  if self then
    local read_float = Process.read_float

    self.x = read_float(address)
    self.y = read_float(address + 0x4)
    self.z = read_float(address + 0x8)
    self.w = read_float(address + 0xC)
  end
end

function Vector4:write(address)
  if self then
    local write_float = Process.write_float

    local wpm1 = write_float(address,       self.x)
    local wpm2 = write_float(address + 0x4, self.y)
    local wpm3 = write_float(address + 0x8, self.z)
    local wpm4 = write_float(address + 0xC, self.w)

    return wpm1 and wpm2 and wpm3 and wpm4
  end
  return false
end

setmetatable(Vector4, {
  __add = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x + rhs
      obj.y = obj.y + rhs
      obj.z = obj.z + rhs
      obj.w = obj.w + rhs
    else
      obj.x = obj.x + rhs.x
      obj.y = obj.y + rhs.y
      obj.z = obj.z + rhs.z
      obj.w = obj.w + rhs.w
    end
    return obj
  end,
  __sub = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x - rhs
      obj.y = obj.y - rhs
      obj.z = obj.z - rhs
      obj.w = obj.w - rhs
    else
      obj.x = obj.x - rhs.x
      obj.y = obj.y - rhs.y
      obj.z = obj.z - rhs.z
      obj.w = obj.w - rhs.w
    end
    return obj
  end,
  __mul = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x * rhs
      obj.y = obj.y * rhs
      obj.z = obj.z * rhs
      obj.w = obj.w * rhs
    else
      obj.x = obj.x * rhs.x
      obj.y = obj.y * rhs.y
      obj.z = obj.z * rhs.z
      obj.w = obj.w * rhs.w
    end
    return obj
  end,
  __div = function(lhs, rhs)
    local obj = deepcopy(lhs)
    if type(rhs) == 'number' then
      obj.x = obj.x / rhs
      obj.y = obj.y / rhs
      obj.z = obj.z / rhs
      obj.w = obj.w / rhs
    else
      obj.x = obj.x / rhs.x
      obj.y = obj.y / rhs.y
      obj.z = obj.z / rhs.z
      obj.w = obj.w / rhs.w
    end
    return obj
  end,
  __tostring = function(lhs)
    return string.format('%.4f | %.4f | %.4f | %.4f', lhs.x, lhs.y, lhs.z, lhs.w)
  end
})

matrix3x4 = {}
for i=1,3 do
  matrix3x4[i] = Vector4.new()
end

function matrix3x4.new(
  m10, m11, m12, m13,
  m20, m21, m22, m23,
  m30, m31, m32, m33
)
  local obj = deepcopy(matrix3x4)
  obj[1]:set(m10, m11, m12, m13)
  obj[2]:set(m20, m21, m22, m23)
  obj[3]:set(m30, m31, m32, m33)
  return obj
end

function matrix3x4:row(index, row_values)
  if self then
    if row_values then
        self[index].x = row_values.x
        self[index].y = row_values.y
        self[index].z = row_values.z
        self[index].w = row_values.w
    end
    return Vector4.new(
      self[index].x,
      self[index].y,
      self[index].z,
      self[index].w
    )
  end
  return nil
end

function matrix3x4:col(index, col_values)
  local get_value = function(this, id)
    if id == 1 then
      return this.x
    elseif id == 2 then
      return this.y
    elseif id == 3 then
      return this.z
    elseif id == 4 then
      return this.w
    end
    return nil
  end

  local set_value = function(this, id, value)
    if id == 1 then
      this.x = value
    elseif id == 2 then
      this.y = value
    elseif id == 3 then
      this.z = value
    elseif id == 4 then
      this.w = value
    end
  end

  if self then
    if col_values then
      set_value(self[1], index, col_values.x)
      set_value(self[2], index, col_values.y)
      set_value(self[3], index, col_values.z)
    else
      return Vector3.new(
        get_value(self[1], index),
        get_value(self[2], index),
        get_value(self[3], index)
      )
    end
  end
  return nil
end

function matrix3x4:set(x, y, z, w)
  if self then
    for i=1,3 do
      self[i]:set(x, y, z, w)
    end
  end
end

function matrix3x4:fill(value)
  if self then
    self:set(value, value, value, value)
  end
end

function matrix3x4:empty()
  if self then
    for i=1,3 do
      if self[i]:empty() ~= true then
        return false
      end
    end
    return true
  end
  return false
end

function matrix3x4:read(address)
  if self then
    for i=1,3 do
      self[i]:read(address + (i - 1) * 0x10)
    end
  end
end

function matrix3x4:write(address)
  if self then
    for i=1,3 do
      self[i]:write(address + (i - 1) * 0x10)
    end
  end
end

setmetatable(matrix3x4, {
  __add = function(lhs, rhs)
    local obj = deepcopy(lhs)
    for i=1,3 do
      obj[i] = obj[i] + rhs
    end
    return obj
  end,
  __sub = function(lhs, rhs)
    local obj = deepcopy(lhs)
    for i=1,3 do
      obj[i] = obj[i] - rhs
    end
    return obj
  end,
  __mul = function(lhs, rhs)
    local obj = deepcopy(lhs)
    for i=1,3 do
      obj[i] = obj[i] * rhs
    end
    return obj
  end,
  __div = function(lhs, rhs)
    local obj = deepcopy(lhs)
    for i=1,3 do
      obj[i] = obj[i] / rhs
    end
    return obj
  end,
  __tostring = function(lhs)
    return string.format(
      '%s\n%s\n%s',
      tostring(lhs[1]),
      tostring(lhs[2]),
      tostring(lhs[3])
    )
  end
})

matrix4x4 = {}
for i=1,4 do
    matrix4x4[i] = Vector4.new()
end

function matrix4x4.new(
  m10, m11, m12, m13,
  m20, m21, m22, m23,
  m30, m31, m32, m33,
  m40, m41, m42, m43
)
  local obj = deepcopy(matrix4x4)
  obj[1]:set(m10, m11, m12, m13)
  obj[2]:set(m20, m21, m22, m23)
  obj[3]:set(m30, m31, m32, m33)
  obj[4]:set(m40, m41, m42, m43)
  return obj
end

function matrix4x4:row(index, row_values)
  if self then
    if row_values then
        self[index].x = row_values.x
        self[index].y = row_values.y
        self[index].z = row_values.z
        self[index].w = row_values.w
    end
    return Vector4.new(
      self[index].x,
      self[index].y,
      self[index].z,
      self[index].w
    )
  end
  return nil
end

function matrix4x4:col(index, col_values)
  local get_value = function(this, id)
    if id == 1 then
      return this.x
    elseif id == 2 then
      return this.y
    elseif id == 3 then
      return this.z
    elseif id == 4 then
      return this.w
    end
    return nil
  end

  local set_value = function(this, id, value)
    if id == 1 then
      this.x = value
    elseif id == 2 then
      this.y = value
    elseif id == 3 then
      this.z = value
    elseif id == 4 then
      this.w = value
    end
  end

  if self then
    if col_values then
      set_value(self[1], index, col_values.x)
      set_value(self[2], index, col_values.y)
      set_value(self[3], index, col_values.z)
      set_value(self[4], index, col_values.w)
    else
      return Vector4.new(
        get_value(self[1], index),
        get_value(self[2], index),
        get_value(self[3], index),
        get_value(self[4], index)
      )
    end
  end
  return nil
end

function matrix4x4:set(x, y, z, w)
  if self then
    for i=1,4 do
      self[i]:set(x, y, z, w)
    end
  end
end

function matrix4x4:fill(value)
  if self then
    self:set(value, value, value, value)
  end
end

function matrix4x4:empty()
  if self then
    for i=1,4 do
      if self[i]:empty() ~= true then
        return false
      end
    end
    return true
  end
  return false
end

function matrix4x4:read(address)
  if self then
    for i=1,4 do
      self[i]:read(address + (i - 1) * 0x10)
    end
  end
end

function matrix4x4:write(address)
  if self then
    for i=1,4 do
      self[i]:write(address + (i - 1) * 0x10)
    end
  end
end

setmetatable(matrix4x4, {
  __add = function(lhs, rhs)
    local obj = deepcopy(lhs)
    for i=1,4 do
      obj[i] = obj[i] + rhs
    end
    return obj
  end,
  __sub = function(lhs, rhs)
    local obj = deepcopy(lhs)
    for i=1,4 do
      obj[i] = obj[i] - rhs
    end
    return obj
  end,
  __mul = function(lhs, rhs)
    local obj = deepcopy(lhs)
    for i=1,4 do
      obj[i] = obj[i] * rhs
    end
    return obj
  end,
  __div = function(lhs, rhs)
    local obj = deepcopy(lhs)
    for i=1,4 do
      obj[i] = obj[i] / rhs
    end
    return obj
  end,
  __tostring = function(lhs)
    return string.format(
      '%s\n%s\n%s\n%s',
      tostring(lhs[1]),
      tostring(lhs[2]),
      tostring(lhs[3]),
      tostring(lhs[4])
    )
  end
})
